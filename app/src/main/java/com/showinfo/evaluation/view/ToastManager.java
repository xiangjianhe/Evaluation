package com.showinfo.evaluation.view;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * @author seven
 */
public class ToastManager {

    public static void showInLong(Context context, String message) {
//		showToastInLong(context,message);
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void showInShort(Context context, String message) {
//		showToastInLong(context,message);
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

}
