package com.showinfo.evaluation.view;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.showinfo.evaluation.R;
import com.showinfo.evaluation.utils.Utils;

import org.xutils.x;

/**
 * @author: limuyang
 * @date: 2019-11-29
 * @Description:
 */
public class Tips {

    private static DragDialog loadingDialog;

    /**
     * 显示 Toast
     *
     * @param message 提示信息
     */
    public static void showShort(String message) {
        showShort(message, Toast.LENGTH_SHORT);
    }

    public static void showLong(String message) {
        showShort(message, Toast.LENGTH_LONG);
    }

    /**
     * 显示 Toast
     *
     * @param message  提示信息
     * @param duration 显示时间长短
     */
    public static void showShort(String message, int duration) {
        x.task().post(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                Toast toast = new Toast(Utils.getContext());
                toast.setDuration(duration);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.setView(createTextToastView(message));
                toast.show();

            }
        });
    }

    public static void showLoading(String message) {
        x.task().post(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                View view = LayoutInflater.from(Utils.getContext()).inflate(R.layout.dialog_loading, null);
                loadingDialog = new DragDialog(Utils.getContext());
                TextView msgText = (TextView) view.findViewById(R.id.tv_loading_dialog_hint);
                msgText.setText(message);
                loadingDialog.setCancelable(true);
                loadingDialog.setContentView(view);
                loadingDialog.show();

            }
        });

    }

    public static void removeLoading() {
        if (loadingDialog != null && loadingDialog.isShowing()) {
            x.task().post(new Runnable() {
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {
                    loadingDialog.dismiss();

                }
            });
        }
    }

    /**
     * 创建自定义 Toast View
     *
     * @param message 文本消息
     * @return View
     */
    private static View createTextToastView(String message) {
        // 画圆角矩形背景
        float rc = dp2px(6);
        RoundRectShape shape = new RoundRectShape(new float[]{rc, rc, rc, rc, rc, rc, rc, rc}, null, null);
        ShapeDrawable drawable = new ShapeDrawable(shape);
        drawable.getPaint().setColor(Color.argb(225, 240, 240, 240));
        drawable.getPaint().setStyle(Paint.Style.FILL);
        drawable.getPaint().setAntiAlias(true);
        drawable.getPaint().setFlags(Paint.ANTI_ALIAS_FLAG);

        // 创建View
        FrameLayout layout = new FrameLayout(Utils.getContext());
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layout.setLayoutParams(layoutParams);
        layout.setPadding(dp2px(16), dp2px(12), dp2px(16), dp2px(12));
        layout.setBackground(drawable);

        TextView textView = new TextView(Utils.getContext());
        textView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
        textView.setTextSize(15);
        textView.setText(message);
        textView.setLineSpacing(dp2px(4), 1f);
        textView.setTextColor(Color.BLACK);

        layout.addView(textView);

        return layout;
    }

    private static int dp2px(float dpValue) {
        final float scale = Utils.getContext().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
