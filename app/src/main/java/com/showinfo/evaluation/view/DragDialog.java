package com.showinfo.evaluation.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Scroller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Copyright (C), 2015-2021
 * FileName: DragDialog
 * Author: seven
 * Date: 21-2-3 下午4:30
 * Description:
 * History:
 */

public class DragDialog extends Dialog implements Handler.Callback {


    private float startx, starty;
    private WindowManager mWindowManager;
    private int oldx, oldy;
    private WindowManager.LayoutParams layoutParams;
    private Scroller scroller;
    private Handler handler;
    float changeX;
    float changeY;
    private int width, height;
    private final static int SCROLLER_HANDLER = 0x711;

    private Window window;

    public DragDialog(@NonNull Context context) {
        super(context);
        init(context);
        this.window = getWindow();
    }

    public DragDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    protected DragDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init(context);
    }

    private void init(Context context) {
        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        scroller = new Scroller(context);
        handler = new Handler(this);
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeigh(int height) {
        this.height = height;
    }

    @Override
    public void dismiss() {
        scroller.abortAnimation();
        handler.removeMessages(SCROLLER_HANDLER);
        super.dismiss();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        layoutParams = (WindowManager.LayoutParams) getWindow().getDecorView().getLayoutParams();
        if (width != 0) {
            layoutParams.width = width;
        }
        if (height != 0) {
            layoutParams.height = height;
        }
        mWindowManager.updateViewLayout(getWindow().getDecorView(), layoutParams);
        oldx = layoutParams.x;
        oldy = layoutParams.y;
    }

    /**
     * 隐藏虚拟栏 ，显示的时候再隐藏掉
     * @param window
     */
    static public void hideNavigationBar(Window window) {
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        window.getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                        //布局位于状态栏下方
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                        //全屏
                        View.SYSTEM_UI_FLAG_FULLSCREEN |
                        //隐藏导航栏
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
                if (Build.VERSION.SDK_INT >= 19) {
                    uiOptions |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                } else {
                    uiOptions |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
                }
                window.getDecorView().setSystemUiVisibility(uiOptions);
            }
        });
    }

    /**
     * dialog 需要全屏的时候用，和clearFocusNotAle() 成对出现
     * 在show 前调用  focusNotAle   show后调用clearFocusNotAle
     * @param window
     */
    static public void focusNotAle(Window window) {
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    /**
     * dialog 需要全屏的时候用，focusNotAle() 成对出现
     * 在show 前调用  focusNotAle   show后调用clearFocusNotAle
     * @param window
     */
    static public void clearFocusNotAle(Window window) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    @Override
    public void show() {
        focusNotAle(window);
        super.show();
        hideNavigationBar(window);
        clearFocusNotAle(window);
    }


//    @Override
//    public boolean onTouchEvent(@NonNull MotionEvent event) {
//
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                startx = event.getX();
//                starty = event.getY();
//                break;
//            case MotionEvent.ACTION_MOVE:
//                changeX = event.getX() - startx;
//                changeY = event.getY() - starty;
//                translateXY(changeX, changeY);
//                break;
//            case MotionEvent.ACTION_UP:
//            case MotionEvent.ACTION_CANCEL:
////                stop();
//                break;
//            default:
//        }
//        return super.onTouchEvent(event);
//    }


    public void stop() {
        scroller.startScroll(layoutParams.x, layoutParams.y, oldx - layoutParams.x, oldy - layoutParams.y);
        handler.obtainMessage(SCROLLER_HANDLER).sendToTarget();
    }

    private void translateXY(float x, float y) {
        scroller.startScroll(layoutParams.x, layoutParams.y, (int) x, (int) y);
        handler.obtainMessage(SCROLLER_HANDLER).sendToTarget();
    }

    @Override
    public boolean handleMessage(Message msg) {
        if (msg.what == SCROLLER_HANDLER && scroller.computeScrollOffset()) {
            layoutParams.x = scroller.getCurrX();
            layoutParams.y = scroller.getCurrY();
            mWindowManager.updateViewLayout(getWindow().getDecorView(), layoutParams);
            handler.obtainMessage(SCROLLER_HANDLER).sendToTarget();
        }
        return false;
    }
}