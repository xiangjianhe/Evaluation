package com.showinfo.evaluation;

import android.app.Application;

import com.showinfo.evaluation.update.AutoUpdateUtil;
import com.showinfo.evaluation.utils.LogToFile;
import com.showinfo.evaluation.utils.Utils;
import com.tencent.bugly.crashreport.CrashReport;


import org.xutils.x;


/**
 * 文 件 名: EvaluationApplication
 * 创 建 人: Allen
 * 创建日期: 16/12/24 15:33
 * 邮   箱: AllenCoder@126.com
 * 修改时间：
 * 修改备注：
 */
public class EvaluationApplication extends Application {
    private static EvaluationApplication appContext;

    public static EvaluationApplication getInstance() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        init();
    }

    private void init() {

        //bugly初始化
        CrashReport.initCrashReport(getApplicationContext(), "3277e239cc", true);
        //初始化xutils
        x.Ext.init(this);
        // 开启debug会影响性能
        x.Ext.setDebug(BuildConfig.DEBUG);
        //初始化下载更新工具
        AutoUpdateUtil.Builder builder = new AutoUpdateUtil.Builder()
                .setBaseUrl("http://192.168.1.74:8112/UpLoadFiles/UpgradeSoft/Showinfo_Update.zip")
                .setIgnoreThisVersion(true)
                .setShowType(AutoUpdateUtil.Builder.TYPE_DIALOG_WITH_PROGRESS)
                .setIconRes(R.mipmap.ic_launcher)
                .showLog(true)
                .setRequestMethod(AutoUpdateUtil.Builder.METHOD_GET)
                .build();
        AutoUpdateUtil.init(builder);

        LogToFile.init();
    }
}
