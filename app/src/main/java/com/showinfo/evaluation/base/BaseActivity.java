package com.showinfo.evaluation.base;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.showinfo.evaluation.http.callback.IConnection;

import java.util.List;


public abstract class BaseActivity extends Activity implements IConnection, View.OnClickListener {

    private static final int REQUEST_EXTERNAL_STORAGE = 0x01;

    /**
     * 获取权限后进行操作
     */
    protected abstract void afterPermissions();

    protected abstract String[] needPermissions();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideBottomUIMenu();


        //防止输入法弹出导致界面上移
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        verifyStoragePermissions(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    public void verifyStoragePermissions(Activity activity) {
        try {
            //检测权限
            if (!checkPermissions(needPermissions())) {
                // 没有写的权限，去申请写的权限，会弹出对话框
                ActivityCompat.requestPermissions(activity, needPermissions(), REQUEST_EXTERNAL_STORAGE);
            } else {
                afterPermissions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkPermissions(String[] neededPermissions) {
        if (neededPermissions == null || neededPermissions.length == 0) {
            return true;
        }
        boolean allGranted = true;
        for (String neededPermission : neededPermissions) {
            allGranted &= ContextCompat.checkSelfPermission(this.getApplicationContext(), neededPermission) == PackageManager.PERMISSION_GRANTED;
        }
        return allGranted;
    }

    @Override
    public void onClick(View v) {

    }
//    protected void hideBottomUIMenu() {
//        //隐藏虚拟按键，并且全屏
//        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
//            View v = this.getWindow().getDecorView();
//            v.setSystemUiVisibility(View.GONE);
//        } else if (Build.VERSION.SDK_INT >= 19) {
//            //for new api versions.
//            View decorView = getWindow().getDecorView();
//            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
//            decorView.setSystemUiVisibility(uiOptions);
//
//        }
//    }

    protected void hideBottomUIMenu() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);//去掉信息栏
        //隐藏虚拟按键，并且全屏
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;
        window.setAttributes(params);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterReceiver(mUsbReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean allGranted = true;
        for (int results : grantResults) {
            allGranted &= results == PackageManager.PERMISSION_GRANTED;
        }
        if (allGranted) {
            afterPermissions();
        } else {
//            Toast.makeText(this, "Maybe can not cache the video or play the local resource!", Toast.LENGTH_LONG).show();
        }
    }

    protected View.OnKeyListener onKey = (v, keyCode, event) -> {

        // TODO Auto-generated method stub

        if (keyCode == KeyEvent.KEYCODE_ENTER) {

            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm.isActive()) {

                imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);

            }

            return true;

        }

        return false;

    };


    /**
     * 根据输入法的状态显示和隐藏输入法
     */
    public static void autoInputmethod(Context context) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

    }

    /**
     * 隐藏软键盘(只适用于Activity，不适用于Fragment)
     */
    protected static void hideSoftKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @Override
    public int loginResultArrive(String result, Object request) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int commonConectResult(String s) {
        return 0;
    }

    @Override
    public void commonConectResult(String result, int connectType) {

    }
}
