package com.showinfo.evaluation.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.showinfo.evaluation.R;
import com.showinfo.evaluation.databinding.ItemPersonBinding;
import com.showinfo.evaluation.entity.Person;
import com.showinfo.evaluation.http.callback.IConnection;
import com.showinfo.evaluation.view.Tips;
import com.showinfo.evaluation.view.DragDialog;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.text.SimpleDateFormat;
import java.util.UUID;

import static com.showinfo.evaluation.http.NetRequestTools.uploadTongJi;


/**
 * @author: seven
 * @date: 2019-12-05
 * @Description: DataBinding Adapter
 */
public class LeftBindingAdapter extends BaseQuickAdapter<Person, BaseDataBindingHolder<ItemPersonBinding>> {

    private DragDialog dragDialog;
    private Context mContext;
    private int type = 0;
    private IConnection iConnection;

    public LeftBindingAdapter(Context context, IConnection parent) {
        super(R.layout.item_person);
        mContext = context;
        iConnection = parent;
        x.task().post(new Runnable() { // UI同步执行
            @Override
            public void run() {
                dragDialog = new DragDialog(context);
            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void convert(@NotNull BaseDataBindingHolder<ItemPersonBinding> holder, Person item) {
        // 获取 Binding
        ItemPersonBinding binding = holder.getDataBinding();
        Log.d("seven", "LeftBindingAdapter-->>convert: " + item.toString());

        if (binding != null) {
            binding.setPerson(item);
            binding.executePendingBindings();
            //让字体滚动
            binding.childScroll.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // Disallow the touch request for parent scroll on touch of child view
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });

            /*
             * 通过ImageOptions.Builder().set方法设置图片的属性
             * 淡入效果
             */
            ImageOptions options = new ImageOptions.Builder().setFadeIn(true).build();

            x.image().bind(binding.personImage, item.getUpdateDate());

            x.image().bind(binding.personImage, item.getUpdateDate(), options, new Callback.CommonCallback<Drawable>() {
                @Override
                public void onSuccess(Drawable result) {
                }

                @Override
                public void onError(Throwable ex, boolean isOnCallback) {
                    binding.personImage.setBackgroundResource(R.drawable.databinding_img);
                }

                @Override
                public void onCancelled(CancelledException cex) {
                }

                @Override
                public void onFinished() {
                }
            });
            //点击非常满意评价
            binding.btnVerysatisfied.setOnClickListener(v -> {
//                binding.submitArea.setVisibility(View.VISIBLE);
//                binding.submitText.setText("是否确定提交非常满意评价");
                type = 1;
                showDialog(type, item);
            });
            //点击满意评价
            binding.btnSatisfied.setOnClickListener(v -> {
//                binding.submitArea.setVisibility(View.VISIBLE);
//                binding.submitText.setText("是否确定提交满意评价");
                type = 2;
                showDialog(type, item);
            });
            //点击一般评价
            binding.btnCommonly.setOnClickListener(v -> {
//                binding.submitArea.setVisibility(View.VISIBLE);
//                binding.submitText.setText("是否确定提交一般评价");
                type = 3;
                showDialog(type, item);

            });
            //点击不满意评价
            binding.btnDissatisfied.setOnClickListener(v -> {
//                binding.submitArea.setVisibility(View.VISIBLE);
//                binding.submitText.setText("是否确定提交不满意评价");
                type = 4;
                showDialog(type, item);
            });
            //点击非常不满意评价
//            binding.btnDissatisfied.setOnClickListener(v -> {
//                binding.submitArea.setVisibility(View.VISIBLE);
//                binding.submitText.setText("是否确定提交非常不满意评价");
//                type = 5;
//            });
            //点击确认，弹出提个人信息对话框
//            binding.submitBtnYes.setOnClickListener(v -> {
//                binding.submitArea.setVisibility(View.GONE);
//                showDialog(type, item);
////                if ((type == 4 || type == 5) && !dragDialog.isShowing()) {
////
////                } else if (!dragDialog.isShowing()) {
////                    doEvaluation(type, item);
////                }
//            });
//            //点击取消
//            binding.submitBtnNo.setOnClickListener(v -> {
//                binding.submitArea.setVisibility(View.GONE);
////                Tips.show("buy ticket: " + item.name + " 取消");
//                type = 0;
//            });

        }
    }

    private void showDialog(int type, Person person) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_report, null);

        EditText name = (EditText) view.findViewById(R.id.text_report_name);
        EditText tel = (EditText) view.findViewById(R.id.text_report_tel);
        EditText caesium = (EditText) view.findViewById(R.id.text_report_case);
        EditText message = (EditText) view.findViewById(R.id.text_report_message);

//        TextView cancel = (TextView) view.findViewById(R.id.btn_cancel);
        ImageView conform = (ImageView) view.findViewById(R.id.btn_conform);
        ImageView cancel = (ImageView) view.findViewById(R.id.btn_cancel);

        name.setOnKeyListener(onKey);
        tel.setOnKeyListener(onKey);
        caesium.setOnKeyListener(onKey);
        message.setOnKeyListener(onKey);

        dragDialog.setContentView(view);
        dragDialog.setWidth(971);
        dragDialog.setHeigh(652);
        //点击取消
//        cancel.setOnClickListener(v -> dragDialog.dismiss());
        //点击确认，将数据上传
        conform.setOnClickListener(v -> {
            if (TextUtils.isEmpty(name.getText())) {
                Tips.showShort("请填写姓名！");
                return;
            } else if (TextUtils.isEmpty(tel.getText())) {
                Tips.showShort("请填写联系方式！");
                return;
            } else if (TextUtils.isEmpty(message.getText())) {
                Tips.showShort("请填写您的评价留言！");
                return;
            }
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddHHmm");
            String date = sDateFormat.format(new java.util.Date());
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("ID", UUID.randomUUID().toString());
                jsonObject.put("PeopleID", person.getPersonId());
                jsonObject.put("VerySatisfied", type == 1);
                jsonObject.put("Satisfied", type == 2);
                jsonObject.put("Commonly", type == 3);
                jsonObject.put("Dissatisfied", type == 4);
                jsonObject.put("BackupColumn", tel.getText().toString());
                jsonObject.put("Name", name.getText().toString());
                jsonObject.put("CreateDate", date);
                jsonObject.put("UpdateDate", message.getText().toString());
                jsonObject.put("CaseNumber", caesium.getText().toString());

                Log.d("seven", "LeftBindingAdapter-->>showDialog: " + jsonObject.toString());
                uploadTongJi(mContext, iConnection, "[" + jsonObject.toString() + "]");
                dragDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        cancel.setOnClickListener(v -> {
            dragDialog.dismiss();
        });

        dragDialog.setCancelable(false);
        dragDialog.show();
    }

    private void doEvaluation(int type, Person person) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        String date = sDateFormat.format(new java.util.Date());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ID", UUID.randomUUID().toString());
            jsonObject.put("PeopleID", person.getPersonId());
            jsonObject.put("VerySatisfied", type == 1);
            jsonObject.put("Satisfied", type == 2);
            jsonObject.put("Commonly", type == 3);
            jsonObject.put("Dissatisfied", type == 4);
            jsonObject.put("BackupColumn", "");
            jsonObject.put("Name", person.getName());
            jsonObject.put("CreateDate", date);
            jsonObject.put("UpdateDate", "");
            jsonObject.put("CaseNumber", "");

            Log.d("seven", "LeftBindingAdapter-->>showDialog: " + jsonObject.toString());
            uploadTongJi(mContext, iConnection, "[" + jsonObject.toString() + "]");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    View.OnKeyListener onKey = (v, keyCode, event) -> {

        // TODO Auto-generated method stub

        if (keyCode == KeyEvent.KEYCODE_ENTER) {

            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm.isActive()) {

                imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);

            }

            return true;

        }

        return false;

    };
}
