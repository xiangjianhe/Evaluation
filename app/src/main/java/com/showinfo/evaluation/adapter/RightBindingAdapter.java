package com.showinfo.evaluation.adapter;


import android.content.Context;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.showinfo.evaluation.R;
import com.showinfo.evaluation.databinding.ItemDepartmentBinding;
import com.showinfo.evaluation.entity.PersonList;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2015-2021
 *
 * @Date: 21-3-3 上午10:50
 * @author: RightBindingAdapter
 * @ClassName: asd
 * @Description:
 **/
public class RightBindingAdapter extends BaseQuickAdapter<PersonList, BaseDataBindingHolder<ItemDepartmentBinding>> {

    private OnItemClickListener listener;
    private Context mContext;

    public RightBindingAdapter(OnItemClickListener listener, Context context) {
        super(R.layout.item_department);
        this.listener = listener;
        mContext = context;
    }

    private final List<TextView> textViews = new ArrayList<>();

    @Override
    protected void convert(@NotNull BaseDataBindingHolder<ItemDepartmentBinding> holder, PersonList item) {
        // 获取 Binding
        ItemDepartmentBinding binding = holder.getDataBinding();

        if (binding != null) {
            binding.btTypeText.setText(item.name);
            textViews.add(binding.btTypeText);
            binding.btTypeText.setSelected(getItemPosition(item) == 0);
            binding.btTypeText.setOnClickListener(v -> {
                listener.select(getItemPosition(item), item.name);
                select(getItemPosition(item));
            });
            binding.executePendingBindings();
        }
    }

    private void select(int position) {
        for (int i = 0; i < getItemCount(); i++) {
            textViews.get(i).setSelected(i == position);
        }
    }

    public interface OnItemClickListener {
        void select(int position, String string);
    }
}
