package com.showinfo.evaluation.http;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;


public class Connect {
	/**
	 * 获取String类型返回内容
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static int CONNECTION_TIMEOUT = 20000;
	
	private static HttpClient httpClient = null;
	 
    private static synchronized HttpClient getHttpClient() {
       if(httpClient == null) {
           httpClient = new DefaultHttpClient();

   		HttpParams params = httpClient.getParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setTcpNoDelay(params, true);
   		HttpConnectionParams.setSoTimeout(params, CONNECTION_TIMEOUT);
   		HttpConnectionParams.setStaleCheckingEnabled(params, false);
       }  
  
      return httpClient;
    }
    

	public static String getContent(String url) {
		try {
			StringBuilder sb = new StringBuilder();

			HttpClient client = getHttpClient();
			HttpResponse response = client.execute(new HttpGet(url));
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(entity.getContent(), "UTF-8"),
						1024);

				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				reader.close();
			}
			return sb.toString();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 服务与后台servlet通讯,加密方法
	 * 
	 * @param url
	 *            通讯地址
	 * @param content
	 *            携带的参数
	 * @return 处理结果，“”表示连接不成功
	 * @throws Exception
	 */
	public static String getFromCipherConnectionSession(String url,
                                                        String content, String sessionID) {
		HttpEntity ent = null;

		System.out.println("URL:"+url);
		HttpPost httpRequest = new HttpPost(url);
		httpRequest.getParams().setBooleanParameter(CoreProtocolPNames.USE_EXPECT_CONTINUE, false);
		
		if (sessionID != null) {
			httpRequest.addHeader("Cookie", "J_SESSIONID=" + sessionID + ";"
					+ "JSESSIONID=" + sessionID);
		}
		HttpResponse httpResponse = null;
		// 绑定到请求 Entry
		StringEntity se = null;
		try {
			se = new StringEntity(content, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		se.setContentType("application/json");
		se.setContentEncoding("UTF-8");
		httpRequest.setEntity(se);
		HttpClient client = getHttpClient();

		int t = 0;
		while (t < 2 && ent == null) {
			try {
				httpResponse = client.execute(httpRequest);
				ent = httpResponse.getEntity();
				// 处理从服务器短来的数据
				if (ent != null) {
					String ret = EntityUtils.toString(ent);
					return ret;
				} else {
					t++;
					Thread.sleep(500);
				}
			} catch (HttpHostConnectException e) {
				e.printStackTrace();
				ent = null;
				t++;
			} catch (Exception e) {
				e.printStackTrace();
				ent = null;
				t++;
			}
		}
		return "";
	}

	/**
	 * 服务与后台servlet通讯,加密方法
	 * 
	 * @param actionUrl
	 *            通讯地址
	 * @param content
	 *            携带的参数
	 * @return 处理结果，“”表示连接不成功
	 * @throws Exception
	 */
	public static String getFromCipherConnection(String actionUrl,
                                                 String content, String path) {

		try {
			File[] files = new File[1];
			files[0] = new File(path);
			// content = Tool.getCipherString(content);
			String BOUNDARY = java.util.UUID.randomUUID().toString();
			String PREFIX = "--", LINEND = "\r\n";
			String MULTIPART_FROM_DATA = "multipart/form-data";
			String CHARSET = "UTF-8";
			URL uri = new URL(actionUrl);
			HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
			conn.setReadTimeout(5 * 1000); // 缓存的最长时间
			conn.setDoInput(true);// 允许输入
			conn.setDoOutput(true);// 允许输出
			conn.setUseCaches(false); // 不允许使用缓存
			conn.setRequestMethod("POST");
			conn.setRequestProperty("connection", "keep-alive");
			conn.setRequestProperty("Charsert", "UTF-8");
			conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA
					+ ";boundary=" + BOUNDARY);
			// 首先组拼文本类型的参数
			StringBuilder sb = new StringBuilder();
			sb.append(PREFIX);
			sb.append(BOUNDARY);
			sb.append(LINEND);
			sb.append("Content-Disposition: form-data; name=\"userName\""
					+ LINEND);
			sb.append("Content-Type: text/plain; charset=" + CHARSET + LINEND);
			sb.append("Content-Transfer-Encoding: 8bit" + LINEND);
			sb.append(LINEND);
			sb.append(content);
			sb.append(LINEND);
			DataOutputStream outStream = new DataOutputStream(
					conn.getOutputStream());
			outStream.write(sb.toString().getBytes());
			// 发送文件数据
			if (files != null) {
				for (File file : files) {
					StringBuilder sb1 = new StringBuilder();
					sb1.append(PREFIX);
					sb1.append(BOUNDARY);
					sb1.append(LINEND);
					sb1.append("Content-Disposition: form-data; name=\""
							+ file.getName() + "\"; filename=\""
							+ file.getName() + "\"" + LINEND);
					sb1.append("Content-Type: application/octet-stream; charset="
							+ CHARSET + LINEND);
					sb1.append(LINEND);
					outStream.write(sb1.toString().getBytes());
					InputStream is = new FileInputStream(file);
					byte[] buffer = new byte[1024];
					int len = 0;
					while ((len = is.read(buffer)) != -1) {
						outStream.write(buffer, 0, len);
					}
					is.close();
					outStream.write(LINEND.getBytes());
				}
			}
			// 请求结束标志
			byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
			outStream.write(end_data);
			outStream.flush();
			outStream.close();
			// 得到响应码
			int res = conn.getResponseCode();
			if (res == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						(InputStream) conn.getInputStream()));
				String line = null;
				StringBuilder result = new StringBuilder();
				while ((line = in.readLine()) != null) {
					result.append(line);
				}
				in.close();
				conn.disconnect();// 断开连接
				String rel = result.toString();
				return rel;
			} else {
				return "";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	/**
	 * 车辆数据提交
	 * @param urlStr
	 * @param textMap
	 * @param fileMap
	 * @return
	 */
	public static String getFromSumbitCarInfo(String urlStr,
                                              Map<String, String> textMap, Map<String, String> fileMap) {
		String res = "";
		HttpURLConnection conn = null;
		String BOUNDARY = "---------------------------123821742118716"; // boundary就是request头和上传文件内容的分隔符
		try {
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
//			if(SysModel.USERINFO.getSessionId() != null){
//				conn.addRequestProperty("Cookie", "JSESSIONID="+SysModel.USERINFO.getSessionId());
//			}
			conn.setConnectTimeout(50000);
			conn.setReadTimeout(30000);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.6)");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data; boundary=" + BOUNDARY);

			OutputStream out = new DataOutputStream(conn.getOutputStream());
			// text
			if (textMap != null) {
				StringBuffer strBuf = new StringBuffer();
				Iterator iter = textMap.entrySet().iterator();
				while (iter.hasNext()) {
					Map.Entry entry = (Map.Entry) iter.next();
					String inputName = (String) entry.getKey();
					String inputValue = (String) entry.getValue();
					System.out.println(inputName+" : "+inputValue);
					if (inputValue == null) {
						continue;
					}
					strBuf.append("\r\n").append("--").append(BOUNDARY)
							.append("\r\n");
					strBuf.append("Content-Disposition: form-data; name=\""
							+ inputName + "\"\r\n\r\n");
					strBuf.append(inputValue);
				}
				out.write(strBuf.toString().getBytes());
			}

			// file
			if (fileMap != null) {
				Iterator iter = fileMap.entrySet().iterator();
				System.out.println("----------------------");
				while (iter.hasNext()) {
					Map.Entry entry = (Map.Entry) iter.next();
					String inputName = (String) entry.getKey();
					String inputValue = (String) entry.getValue();
					
					System.out.println(inputName+"  :  "+inputValue);
					if (inputValue == null) {
						continue;
					}
					File file = new File(inputValue);
					String filename = file.getName();
					System.out.println("filename  " +filename);
//					String contentType = FileUtils.getMimeType(urlStr);
					String contentType = "";
					if (filename.endsWith(".png")) {
						contentType = "image/png";
					}
					if(filename.endsWith(".jpg")){
						contentType = "image/jpg";
					}
					if (contentType == null || contentType.equals("")) {
						contentType = "application/octet-stream";
					}

					StringBuffer strBuf = new StringBuffer();
					strBuf.append("\r\n").append("--").append(BOUNDARY)
							.append("\r\n");
					strBuf.append("Content-Disposition: form-data; name=\""
							+ inputName + "\"; filename=\"" + filename
							+ "\"\r\n");
					strBuf.append("Content-Type:" + contentType + "\r\n\r\n");

					out.write(strBuf.toString().getBytes());

					DataInputStream in = new DataInputStream(
							new FileInputStream(file));
					int bytes = 0;
					byte[] bufferOut = new byte[1024];
					while ((bytes = in.read(bufferOut)) != -1) {
						out.write(bufferOut, 0, bytes);
					}
					in.close();
				}
			}

			byte[] endData = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();
			out.write(endData);
			out.flush();
			out.close();
			
			// 读取返回数据
			StringBuffer strBuf = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line = null;
			while ((line = reader.readLine()) != null) {
				strBuf.append(line).append("\n");
			}
			res = strBuf.toString();
			reader.close();
			reader = null;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
				conn = null;
			}
		}
		
//		try {
//			res = CipherTool.getOriginString(res);
//			System.out.println(res);
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return res;
	}
}
