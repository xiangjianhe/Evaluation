package com.showinfo.evaluation.http;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

import java.io.UnsupportedEncodingException;

public class RpcHttp {

    public static int CONNECTION_TIMEOUT = 20000;

    private static HttpClient customerHttpClient;

    private static synchronized HttpClient getHttpClient() {
        if (customerHttpClient == null) {
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
            HttpConnectionParams.setTcpNoDelay(params, true);
            HttpConnectionParams.setSoTimeout(params, CONNECTION_TIMEOUT);
            HttpConnectionParams.setStaleCheckingEnabled(params, false);

            SchemeRegistry schReg = new SchemeRegistry();
            schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);
            customerHttpClient = new DefaultHttpClient(conMgr, params);
        }
        return customerHttpClient;
    }

    public static String getFromCipherConnectionSession(String url, String content, String token) {
        HttpEntity ent = null;

        System.out.println("URL:" + url);
        HttpPost httpRequest = new HttpPost(url);
        httpRequest.getParams().setBooleanParameter(
                CoreProtocolPNames.USE_EXPECT_CONTINUE, false);

        if (token != null) {
            httpRequest.addHeader("Access-Token", token);
        }
        HttpResponse httpResponse = null;
        // 绑定到请求 Entry
        StringEntity se = null;
        try {
            se = new StringEntity(content, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        se.setContentType("application/json");
        se.setContentEncoding("UTF-8");
        httpRequest.setEntity(se);
        HttpClient client = getHttpClient();
        int t = 0;
        while (t < 2 && ent == null) {
            try {
                httpResponse = client.execute(httpRequest);
                ent = httpResponse.getEntity();
                int code = httpResponse.getStatusLine().getStatusCode();
                if (code == 200) {

                    Log.d("hxj", "RpcHttp: getFromCipherConnectionSession: code is " + code);
                    // 处理从服务器短来的数据
                    if (ent != null) {
                        String ret = EntityUtils.toString(ent);
                        return ret;
                    } else {
                        t++;
                        Thread.sleep(500);
                    }
                } else {
                    return "" + code;
                }
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                ent = null;
                t++;
            } catch (Exception e) {
                e.printStackTrace();
                ent = null;
                t++;
            }
        }
        return "";
    }

}
