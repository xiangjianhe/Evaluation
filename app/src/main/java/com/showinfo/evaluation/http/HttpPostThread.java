package com.showinfo.evaluation.http;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.showinfo.evaluation.http.callback.IConnection;


public class HttpPostThread implements Runnable {

	private int connectType;
	private IConnection parent;
	private String url;
	private String params;
	private String result = "";
	private boolean isLocal = false;
	private Context context;
	private String token;

    public HttpPostThread(Context context, IConnection parent, String url, String content, int connectType) {
		this.context = context;
    	this.connectType = connectType;
    	this.parent = parent;
    	this.params = content;
    	this.url = url;
    }
	public HttpPostThread(Context context, IConnection parent, String url, String content, int connectType, String token) {
		this.context = context;
		this.connectType = connectType;
		this.parent = parent;
		this.params = content;
		this.url = url;
		this.token=token;
	}
    
    public HttpPostThread(IConnection parent, int connectType, String url, String content) {
    	this.connectType = connectType;
    	this.parent = parent;
    	this.params = content;
    	this.url = url;
    }

    @Override
    public void run() {
        try {
        	System.out.println("post:"+url);
        	result = getLocalData();
        	if(!isLocal){

				if(!TDevice.hasInternet(context)){
					result = "nonetwork";
				}else{
					if (token!=null) {
						result = RpcHttp.getFromCipherConnectionSession(url, params, token);
					}else {
						result = RpcHttp.getFromCipherConnectionSession(url, params, null);
					}
				}
				result = GetLocalData.getInstance().getResult(connectType, result,this.params);
			}
        } catch(Exception e){
            e.printStackTrace();
        }
        finally {
        	new Handler(Looper.getMainLooper()).post(new Runnable() {
				@Override
				public void run() {
					if(parent!=null){
						try {
							parent.commonConectResult(result == null ? "": result, connectType);
						} catch (Exception e2) {
							e2.printStackTrace();
//							ProgressManager.closeProgress();
						}
					}
				}
			});//在子线程中直接去new 一个handler
        }
    }
    public String getLocalData(){
		return null;
    }
}
