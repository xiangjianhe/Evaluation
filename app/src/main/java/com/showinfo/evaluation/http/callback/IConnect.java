package com.showinfo.evaluation.http.callback;

/**
 * Copyright (C), 2015-2021
 *
 * @Date: 21-3-16 下午3:04
 * @author: seven
 * @ClassName: IConnect
 * @Description:
 **/
public interface IConnect {
    /**
     * 执行回调
     */
    void dataChange();

    void addShutdown();

    void doClean();
}
