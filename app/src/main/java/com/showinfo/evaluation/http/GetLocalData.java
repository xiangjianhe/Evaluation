package com.showinfo.evaluation.http;

import android.content.Context;

import org.xutils.DbManager;



public class GetLocalData {

    private static Context mContext;
    private static DbManager db;

    public static GetLocalData localData;

    public static GetLocalData getInstance() {
        if (localData != null) {
            return localData;
        }
        localData = new GetLocalData();
        mContext = SysApp.getInstance();
        return localData;
    }

    public String getResult(int connectType, String result, String params) {
        if ("".equals(result)) {
            return result;
        }
        switch (connectType) {
            default:
                if ("nonetwork".equals(result)) {
                    result = CacheManager.readObject(mContext, connectType + "_" + params);
                }
                break;
        }
        return result;
    }
}
