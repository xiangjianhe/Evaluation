package com.showinfo.evaluation.http;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.util.Log;


import com.showinfo.evaluation.base.BaseApplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//import com.squareup.leakcanary.LeakCanary;


public class SysApp extends BaseApplication {
    //

    public List<Activity> activityList = new ArrayList<Activity>();
    public List<Service> serviceList = new ArrayList<Service>();

    public static SysApp sysApp;

    public static SysApp getInstance() {
        return sysApp;
    }

    private boolean isRunning = false;

    public boolean getIsAppRunning() {
        return isRunning;
    }

    private static final String PROC_CPU_INFO_PATH = "/proc/cpuinfo";
    private static boolean LOGENABLE = false;

    @Override
    public void onCreate() {
        super.onCreate();

        //内存泄漏检查
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//// This process is dedicated to LeakCanary for
//            // heap analysis.
//            // You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);


//		initImageLoader(this);
        // 融云初始化
//        WXUtil.initWx(this);
//        try {
//            RongPushClient.registerHWPush(this);
////            RongPushClient.checkManifest(this);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        try {
//            RongIM.init(this);
//        }catch (Exception e){
//
//        }
    }

    public static void setProperty(String key, String value) {
        Editor editor = getPreferences().edit();
        editor.putString(key, value);
        apply(editor);
    }

    public static String getProperty(String key) {
        return getPreferences().getString(key, null);
    }

    public static void removeProperty(String... keys) {
        for (String key : keys) {
            Editor editor = getPreferences().edit();
            editor.putString(key, null);
            apply(editor);
        }
    }


    public static String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager
                .getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

    private static String getSystemProperty(String key, String defaultValue) {
        String value = defaultValue;
        try {
            Class<?> clazz = Class.forName("android.os.SystemProperties");
            Method get = clazz.getMethod("get", String.class, String.class);
            value = (String) (get.invoke(clazz, key, ""));
        } catch (Exception e) {
            if (LOGENABLE) {
                Log.d("getSystemProperty", "key = " + key + ", error = " + e.getMessage());
            }
        }

        if (LOGENABLE) {
            Log.d("getSystemProperty", key + " = " + value);
        }
        return value;
    }

    private static boolean isCPUInfo64() {
        File cpuInfo = new File(PROC_CPU_INFO_PATH);
        if (cpuInfo != null && cpuInfo.exists()) {
            InputStream inputStream = null;
            BufferedReader bufferedReader = null;
            try {
                inputStream = new FileInputStream(cpuInfo);
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 512);
                String line = bufferedReader.readLine();
                if (line != null && line.length() > 0 && line.toLowerCase(Locale.US).contains("arch64")) {
                    if (LOGENABLE) {
                    }
                    return true;
                } else {
                    if (LOGENABLE) {
                    }
                }
            } catch (Throwable t) {
                if (LOGENABLE) {
                }
            } finally {
                try {
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }


}