package com.showinfo.evaluation.http.callback;


public interface IConnection {

    public int loginResultArrive(String result, Object request);

    /**
     * 登陆等相关接口
     *
     * @param s
     * @return
     */
    public int commonConectResult(String s);

    public void commonConectResult(String result, int connectType);

}
