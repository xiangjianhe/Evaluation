package com.showinfo.evaluation.http;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.TextView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.showinfo.evaluation.EvaluationApplication;
import com.showinfo.evaluation.R;
import com.showinfo.evaluation.activity.MainActivity;
import com.showinfo.evaluation.entity.MainProcess;
import com.showinfo.evaluation.entity.Person;
import com.showinfo.evaluation.entity.ShutdownTime;
import com.showinfo.evaluation.http.callback.IConnect;
import com.showinfo.evaluation.http.callback.IConnection;
import com.showinfo.evaluation.update.AutoUpdateUtil;
import com.showinfo.evaluation.utils.LogToFile;
import com.showinfo.evaluation.utils.SharefenceUtil;
import com.showinfo.evaluation.view.Tips;
import com.showinfo.evaluation.utils.Utils;

import org.json.JSONException;
import org.xutils.DbManager;
import org.xutils.ex.DbException;

import org.xutils.x;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.showinfo.evaluation.activity.MainActivity.getHeight;
import static com.showinfo.evaluation.activity.MainActivity.getWith;
import static com.showinfo.evaluation.utils.NetUtils.getLocalIp;
import static com.showinfo.evaluation.utils.NetUtils.getMac;
import static com.showinfo.evaluation.utils.Utils.bitmapToBase64;
import static com.showinfo.evaluation.utils.Utils.capture;
import static com.showinfo.evaluation.utils.Utils.execSuCmd;
import static com.showinfo.evaluation.utils.Utils.getSN;
import static com.showinfo.evaluation.utils.Utils.getVersion;
import static com.showinfo.evaluation.utils.Utils.reboot;
import static com.showinfo.evaluation.utils.Utils.restartApp2;
import static com.showinfo.evaluation.utils.Utils.shutdown;


/**
 * Copyright (C), 2015-2021
 *
 * @Date: 21-3-16 下午2:53
 * @author: seven
 * @ClassName: NetRequestTools
 * @Description: 执行网络请求任务
 **/
public class NetRequestTools {

    public static String macId = getMac(EvaluationApplication.getInstance());
    public static final int COMMAND_SUCESS = 0;

    /**
     * 数据库
     */
    public static DbManager.DaoConfig daoConfig = new DbManager.DaoConfig()
            .setDbName("person.db")
            // 不设置dbDir时, 默认存储在app的私有目录.
//            .setDbDir(new File("/sdcard")) // "sdcard"的写法并非最佳实践, 这里为了简单, 先这样写了.
            .setDbVersion(2)
            .setDbOpenListener(new DbManager.DbOpenListener() {
                @Override
                public void onDbOpened(DbManager db) {
                    // 开启WAL, 对写入加速提升巨大
                    db.getDatabase().enableWriteAheadLogging();
                }
            })
            .setDbUpgradeListener(new DbManager.DbUpgradeListener() {
                @Override
                public void onUpgrade(DbManager db, int oldVersion, int newVersion) {
                    // TODO: ...
                    // db.addColumn(...);
                    // db.dropTable(...);
                    // ...
                    // or
                    // db.dropDb();
                }
            });


    /**
     * 提交新终端信息 接口
     *
     * @param mac
     * @param parent
     */
    private static void newMacQuery(String mac, IConnect parent) {
        LogToFile.d("seven", "MainActivity-->>newMacQuery: ");
        // 编码集
        final MediaType mediaType = MediaType.parse("multipart/form-data");
        String ip = SharefenceUtil.getIp(EvaluationApplication.getInstance());
        String duan = SharefenceUtil.getDuan(EvaluationApplication.getInstance());
        // 接口地址
        final String uri = "http://" + ip + ":" + duan + "/Service/NewMacQuery";


        // 创建实例
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .build();

        LogToFile.d("seven", "NetRequestTools-->>newMacQuery: macId is " + macId + " name is " + Build.MODEL);
        // 创建表单及数据
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("FMacNum", macId)
                .addFormDataPart("FMacName", Build.MODEL)
                .addFormDataPart("FMacRX", "" + getWith())
                .addFormDataPart("FMacRY", "" + getHeight())
                .addFormDataPart("FMacSoftEdition", getVersion())
                .addFormDataPart("FMacType", "android")
                .addFormDataPart("FMacIP", getLocalIp())
                .addFormDataPart("FMacMySN", getSN())
                .build();

        // 创建请求实例
        Request request = new Request.Builder()
                .url(uri)
                .method("POST", requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogToFile.e("seven", "接口调用失败");
                Tips.showShort(EvaluationApplication.getInstance().getString(R.string.disconnect_server_tip));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String result = response.body().string();
                JSONObject json2 = JSONObject.parseObject(result);
                if (!json2.getString("code").equals("0")) {
                    return;
                }
                WcfMacMain(mac, parent);

            }
        });
    }


    /**
     * 获取任务id
     *
     * @param mac
     * @param parent
     */
    public static void WcfMacMain(String mac, IConnect parent) {
        // 编码集
        final MediaType mediaType = MediaType.parse("multipart/form-data");
        String ip = SharefenceUtil.getIp(EvaluationApplication.getInstance());
        String duan = SharefenceUtil.getDuan(EvaluationApplication.getInstance());
        // 接口地址
        final String uri = "http://" + ip + ":" + duan + "/Service/WcfMacMain";
        LogToFile.d("seven", "NetRequestTools-->>newMacQuery: uri is " + uri);
        // 创建实例
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .build();

        // 创建表单及数据
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("MacNum", mac)
                .build();

        // 创建请求实例
        Request request = new Request.Builder()
                .url(uri)
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogToFile.e("seven", "接口调用失败");
                Tips.showShort(EvaluationApplication.getInstance().getString(R.string.disconnect_server_tip));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String result = response.body().string();
                JSONObject json2 = JSONObject.parseObject(result);
                if (!json2.getString("code").equals("0")) {
                    return;
                }

                String data = json2.getString("data");
                List<MainProcess> mainProcesses = (ArrayList<MainProcess>) JSONArray.parseArray(data, MainProcess.class);

                if (mainProcesses.size() != 0) {
                    for (int i = 0; i < mainProcesses.size(); i++) {
                        MainProcess ms = mainProcesses.get(i);
                        doJob(i == (mainProcesses.size() - 1), i == 0, ms, parent);
                    }
                }
            }
        });
    }

    /**
     * 根据指令执行任务
     * 操作类型：
     * 1：查询成功 为新增设备，请求NewMacQuery接口提交设备信息
     * 2：查询成功 为已有设备且没有任务，无需后续操作
     * 3：同步时间
     * 6：设置音量，OData中为0-100音量数值
     * b：清理终端文件
     * d：上传截屏图片，请求UploadPrtScreen接口，OData中为 截屏id
     * g：重启终端
     * h：关闭终端
     * r：升级终端软件
     * t：清理终端文件
     * z：重启终端软件
     * Synctime：同步终端时间
     * uptongji：提交评价结果，请求UploadTongJi接口
     * addpeople：增加一条人员数据，请求GetYuanGong接口，OData中为人员id
     * delpeople：删除一条人员数据，请求GetYuanGong接口 ，OData中为人员id
     * editpeople：编辑一条人员数据，请求GetYuanGong接口，OData中为人员id
     *
     * @param mp
     * @param parent
     */
    public static void doJob(boolean isLast, boolean isFirst, MainProcess mp, IConnect parent) {
        LogToFile.i("seven", "NetRequestTools-->>doJob: mp.getOType() " + mp.getOType() + " task " + mp.getTaskID() + "  data " + mp.getOData());
        switch (mp.getOType()) {
            case "1":
                newMacQuery(macId, parent);
                break;
            case "2":
                break;
            case "3":
                //同步终端时间
            case "Synctime":
                getSynctime(mp.getTaskID());
                break;
            case "6":
                setVolume(mp.getOData());
                retOperatResult(mp.getTaskID());
                break;
            case "d":
                uploadPrtScreen(mp.getOData(), macId, bitmapToBase64(capture((Activity) MainActivity.mContext)));
                retOperatResult(mp.getTaskID());
                break;
            case "r":
                doUpdate(mp.getTaskID());
                break;
            case "t":
                doClean(parent);
                retOperatResult(mp.getTaskID());
                break;
            case "g":
                retOperatResult(mp.getTaskID());
                reboot();
                break;
            case "h":
                retOperatResult(mp.getTaskID());
                shutdown();
                break;
            //重启终端软件
            case "z":
                retOperatResult(mp.getTaskID());
                restartApp2();
                break;
            //提交评价结果，请求UploadTongJi接口
            case "uptongji":
                retOperatResult(mp.getTaskID());
                break;
            case "Shutdowntime":
                try {
                    DbManager db = x.getDb(daoConfig);
                    if (isFirst) {
                        //删除原有的
                        db.dropTable(ShutdownTime.class);
                    }
                    //6|00:02
                    String data = mp.getOData();
                    Log.d("seven", "NetRequestTools --->> doJob: data is " + data);
                    ShutdownTime time = new ShutdownTime();
                    time.setWeekday(Integer.parseInt(data.substring(0, data.lastIndexOf("|"))));
                    time.setTime(data.substring(data.indexOf("|") + 1));
                    db.save(time);
                    Log.e("seven", "NetRequestTools --->> doJob: " + time.toString());
                    if (isLast) {
                        parent.addShutdown();
                    }
                    retOperatResult(mp.getTaskID());
                } catch (DbException e) {
                    e.printStackTrace();
                }


                retOperatResult(mp.getTaskID());
                break;
            //增加一条人员数据，请求GetYuanGong接口，OData中为人员id
            case "addpeople":
                getYuanGong(isLast, 1, mp, parent);
                break;
            //编辑一条人员数据，请求GetYuanGong接口，OData中为人员id
            case "editpeople":
                getYuanGong(isLast, 2, mp, parent);
                break;
            //删除一条人员数据，请求GetYuanGong接口 ，OData中为人员id
            case "delpeople":
                try {
                    DbManager db = x.getDb(daoConfig);
                    //删除
                    Person people = db.selector(Person.class).where("personId", "in", new String[]{mp.getOData()}).findFirst();
                    if (people != null) {
                        db.delete(people);
                    }
                    parent.dataChange();
                    retOperatResult(mp.getTaskID());
                } catch (DbException e) {
                    e.printStackTrace();
                }

//                getYuanGong(isLast, 3, mp, parent);
                break;

            default:
                break;
        }

    }

    private static void doClean(IConnect parent) {
        try {
            DbManager db = x.getDb(daoConfig);

            db.dropTable(Person.class);
            parent.doClean();
        } catch (DbException e) {
            e.printStackTrace();
        }
    }


    /**
     * 同步终端时间
     *
     * @param id 任务id
     */
    public static void getSynctime(String id) {
        // 编码集
        final MediaType mediaType = MediaType.parse("multipart/form-data");
        String ip = SharefenceUtil.getIp(EvaluationApplication.getInstance());
        String duan = SharefenceUtil.getDuan(EvaluationApplication.getInstance());
        // 接口地址
        final String uri = "http://" + ip + ":" + duan + "/Service/GetSynctime";

        // 创建实例
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .build();

        // 创建表单及数据
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("OperatID", id)
                .build();

        // 创建请求实例
        Request request = new Request.Builder()
                .url(uri)
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("seven", "接口调用失败");
                Tips.showShort(EvaluationApplication.getInstance().getString(R.string.disconnect_server_tip));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String result = response.body().string();
                JSONObject json = JSONObject.parseObject(result);
                String data = json.getString("data");//"2021/04/15 15:46:14"2021/03/19 15:20
                String y, m, d, o, min, second;
                retOperatResult(id);
                try {
                    y = data.substring(0, 4);
                    m = data.substring(5, 7);
                    d = data.substring(8, 10);
                    o = data.substring(data.length() - 8, data.length() - 6);
                    min = data.substring(data.length() - 5, data.length() - 3);
                    second = data.substring(data.length() - 2);
                    //命令格式：date MMddHHmmyyyy.ss set
                    //（月日时分年.秒）41515592021.35
                    Log.d("seven", "MainActivity-->>isToday: y is " + y + " m is " + m + " d is " + d + " o is " + o + " min is " + min + "second " + second);
                    String curr_time = "" + m + d + o + min + y + "." + second;
                    Log.d("seven", "NetRequestTools-->>onResponse: curr_time is " + curr_time);
                    execSuCmd("date " + curr_time + "\n busybox hwclock -w\n");
                    Log.d("seven", "NetRequestTools-->>onResponse: result is " + result);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
    }

    /**
     * 下载并升级终端
     *
     * @param id 任务id
     */
    private static void doUpdate(String id) {
        Tips.showLong("正在升级终端，请稍候。。。");
        retOperatResult(id);
        String ip = SharefenceUtil.getIp(EvaluationApplication.getInstance());
        String duan = SharefenceUtil.getDuan(EvaluationApplication.getInstance());
        // 接口地址
        final String uri = "http://" + ip + ":" + duan + "/UpLoadFiles/UpgradeSoft/Showinfo_Update.zip";
        AutoUpdateUtil.getInstance(MainActivity.mContext).downLoadFile(uri);
    }

    /**
     * 设置音量
     *
     * @param volume 音量大小
     */
    private static void setVolume(String volume) {
        AudioManager audioManager = (AudioManager) MainActivity.mContext.getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = 15 * Integer.parseInt(volume) / 100;

        LogToFile.d("seven", "MainActivity-->>initClicked: currentVolume is " + currentVolume);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, AudioManager.FLAG_PLAY_SOUND);
        x.task().post(new Runnable() {
            @Override
            public void run() {
                double cangweibaifenbi = Double.parseDouble(volume) / 100;
                DecimalFormat df = new DecimalFormat("00.00%");
                Tips.showShort("已将音量设置为" + df.format(cangweibaifenbi));
            }
        });
    }

    /**
     * 上传截图
     *
     * @param cutimgId  截屏任务中的 OData值
     * @param macNum    终端设备标识
     * @param base64img 截屏图片（jpeg格式）的base64字符串
     */
    public static void uploadPrtScreen(String cutimgId, String macNum, String base64img) {
        // 编码集
        final MediaType mediaType = MediaType.parse("multipart/form-data");
        String ip = SharefenceUtil.getIp(EvaluationApplication.getInstance());
        String duan = SharefenceUtil.getDuan(EvaluationApplication.getInstance());
        // 接口地址
        final String uri = "http://" + ip + ":" + duan + "/Service/UploadPrtScreen";

        // 创建实例
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .build();

        // 创建表单及数据
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("cutimgId", cutimgId)
                .addFormDataPart("macNum", macNum)
                .addFormDataPart("base64img", "data:image/jpeg;base64," + base64img)
                .build();

        // 创建请求实例
        Request request = new Request.Builder()
                .url(uri)
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogToFile.e("seven", "接口调用失败");
                Tips.showShort(EvaluationApplication.getInstance().getString(R.string.disconnect_server_tip));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String result = response.body().string();
                LogToFile.d("seven", "NetRequestTools-->uploadPrtScreen>onResponse: result is " + result);
                retOperatResult(cutimgId);
            }
        });
    }


    /**
     * 获取人员信息
     *
     * @param isLast
     * @param type
     * @param mp
     * @param parent
     */
    public static void getYuanGong(boolean isLast, int type, MainProcess mp, IConnect parent) {
        LogToFile.d("seven", "NetRequestTools-->>getYuanGong: ");
        // 编码集
        final MediaType mediaType = MediaType.parse("multipart/form-data");
        String ip = SharefenceUtil.getIp(EvaluationApplication.getInstance());
        String duan = SharefenceUtil.getDuan(EvaluationApplication.getInstance());
        // 接口地址
        final String uri = "http://" + ip + ":" + duan + "/Service/GetYuanGong";

        // 创建实例
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .build();

        // 创建表单及数据
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("id", mp.getOData())
                .build();

        // 创建请求实例
        Request request = new Request.Builder()
                .url(uri)
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogToFile.e("seven", "接口调用失败");
                Tips.showShort(EvaluationApplication.getInstance().getString(R.string.disconnect_server_tip));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String result = response.body().string();
                JSONObject json = JSONObject.parseObject(result);
                JSONObject object = json.getJSONObject("data");
                if (!json.getString("code").equals("0")) {
                    return;
                }
                LogToFile.d("seven", "NetRequestTools-->getYuanGong>onResponse: object is " + object.toString());
                try {
                    DbManager db = x.getDb(daoConfig);

                    Person p = new Person();
                    p.setPersonId(object.getString("ID"));
                    p.setBackupColumn(object.getString("BackupColumn"));
                    p.setCreateDate(object.getString("CreateDate"));
                    p.setDepartment(object.getString("Department"));
                    p.setIndexS(object.getIntValue("Index_S"));
                    p.setInfo(object.getString("Info"));
                    p.setMacId(object.getString("MacID"));
                    p.setName(object.getString("Name"));
                    p.setPost(object.getString("Post"));
                    p.setTelphone(object.getString("Telphone"));
                    p.setUpdateDate(object.getString("UpdateDate"));
                    p.setTaskId(mp.getTaskID());

                    Person people = db.selector(Person.class).where("personId", "in", new String[]{p.getPersonId()}).findFirst();
                    if (type == 1) {
                        LogToFile.d("seven", "NetRequestTools-->>onResponse: " + p.toString());

                        if (people == null) {
                            //添加
//                            Tips.showShort();("已将" + p.getName() + "存储到数据库！");

                            db.save(p);
                        } else {
                            LogToFile.d("seven", "NetRequestTools-->>onResponse: " + people.toString());
                        }
                    } else if (type == 2) {
                        //更新

                        if (people != null) {
                            p.setId(people.getId());
                            db.update(p);
                        }

                    }

                    retOperatResult(mp.getTaskID());
                    if (isLast) {
//                        Tips.showShort("一共接受到" + (count + 1) + "个数据");
                        parent.dataChange();
                    }
                } catch (DbException e) {
                    e.printStackTrace();
                    Log.d("seven", "NetRequestTools-->>onResponse: " + e.getStackTrace().toString());
                }


            }
        });
    }

    /**
     * 提交任务执行结果 接口
     *
     * @param id 任务id
     */
    public static void retOperatResult(String id) {
        // 编码集
        final MediaType mediaType = MediaType.parse("multipart/form-data");
        String ip = SharefenceUtil.getIp(EvaluationApplication.getInstance());
        String duan = SharefenceUtil.getDuan(EvaluationApplication.getInstance());
        // 接口地址
        final String uri = "http://" + ip + ":" + duan + "/Service/RetOperatResult";

        // 创建实例
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .build();

        // 创建表单及数据
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("OperatID", id)
                .build();

        // 创建请求实例
        Request request = new Request.Builder()
                .url(uri)
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogToFile.e("seven", "接口调用失败");
                Tips.showShort(EvaluationApplication.getInstance().getString(R.string.disconnect_server_tip));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String result = response.body().string();
                LogToFile.d("seven", "NetRequestTools-->>onResponse: result is " + result);

            }
        });
    }

    /**
     * 提交评价数据 (json)
     *
     * @param context      参数
     * @param parent       回调接口
     * @param tongjijieguo 评价数据
     * @return
     */
    public static int uploadTongJi(Context context, IConnection parent, String tongjijieguo) {
        String ip = SharefenceUtil.getIp(EvaluationApplication.getInstance());
        String duan = SharefenceUtil.getDuan(EvaluationApplication.getInstance());
        String uri = "http://" + ip + ":" + duan + "/Service/UploadTongJi_json";
        org.json.JSONObject obj = new org.json.JSONObject();
        LogToFile.d("seven", "NetRequestTools-->>getWav: " + tongjijieguo);
        String content = "";
        try {
            obj.put("tongjijieguo", tongjijieguo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        content = obj.toString();
        Log.d("seven", "NetRequestTools-->>uploadTongJi: " + tongjijieguo);
        ThreadPoolUtils.execute(new HttpPostThread(context, parent, uri, tongjijieguo,
                SysConst.GET_COUNT_RESULT));

        return COMMAND_SUCESS;
    }

    /**
     * 获取满意率 接口
     *
     * @param textView 显示满意率的view
     */
    public static void getCountResult(TextView textView) {
        // 编码集
        final MediaType mediaType = MediaType.parse("multipart/form-data");
        String ip = SharefenceUtil.getIp(EvaluationApplication.getInstance());
        String duan = SharefenceUtil.getDuan(EvaluationApplication.getInstance());
        // 接口地址
        final String uri = "http://" + ip + ":" + duan + "/Service/GetCountResult";

        // 创建实例
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .build();

        // 创建表单及数据
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("FMacNum", getMac(Utils.getContext()))
                .build();

        // 创建请求实例
        Request request = new Request.Builder()
                .url(uri)
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogToFile.e("seven", "接口调用失败");
                Tips.showShort(EvaluationApplication.getInstance().getString(R.string.disconnect_server_tip));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String result = response.body().string();
                JSONObject json1 = JSONObject.parseObject(result);
                if ("0".equals(json1.getString("code"))) {
                    String rate = json1.getString("data");
                    //获取到音频数据后
                    if (json1.getString("data") != null && !"".equals(rate)) {

                        x.task().post(new Runnable() { // UI同步执行
                            @Override
                            public void run() {
                                //获取年月日
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy/MM/dd");
                                String date = sDateFormat.format(new java.util.Date());
                                //将返回的string满意率类型转换为百分比
                                double rates = Double.parseDouble(rate);
                                DecimalFormat df = new DecimalFormat("00.00%");

                                textView.setText("截止" + date + "日为止，全院满意评价达到" + df.format(rates));
                            }
                        });

                    }
                }
            }
        });
    }

}