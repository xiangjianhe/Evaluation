package com.showinfo.evaluation.entity;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * Copyright (C), 2015-2021
 *
 * @Date: 2021/5/27 上午11:36
 * @author: seven
 * @ClassName: ShutdownTime
 * @Description:
 **/
@Table(name = "shutdownTime")
public class ShutdownTime {
    @Column(name = "id", isId = true)
    private int id;
    @Column(name = "weekday")
    private String weekday;
    @Column(name = "time")
    private String time;

    public static String[] week = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(int weekday) {
        this.weekday = week[weekday];
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "ShutdownTime{" +
                "id=" + id +
                ", weekday=" + weekday +
                ", time='" + time + '\'' +
                '}';
    }
}
