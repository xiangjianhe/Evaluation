package com.showinfo.evaluation.entity;

import java.util.List;

/**
 * Copyright (C), 2015-2021
 *
 * @Date: 21-3-15 下午2:40
 * @author: seven
 * @ClassName: Process 任务流程
 * @Description:
 **/
public class MainProcess {

    /**
     * code : 0
     * msg :
     * data : [{"TaskID":"6af08fe5ad7b42d89c7d7e220c4cd915","OType":"uptongji","OData":""},{"TaskID":"e0530f79986148d487eb26eb351f0982","OType":"uptongji","OData":""},{"TaskID":"9778522f2c70465ea9e5e0a1013089c0","OType":"uptongji","OData":""},{"TaskID":"12c011e6eef1405a91ba10d5832b09bd","OType":"uptongji","OData":""},{"TaskID":"5dd0dde064f34861938c46af86d69a4c","OType":"uptongji","OData":""},{"TaskID":"7ef5c1d527894b8093fd6540bd8dac08","OType":"uptongji","OData":""},{"TaskID":"2cfe2e1432fb410f9c817e4672e6c161","OType":"uptongji","OData":""},{"TaskID":"adccc62e8594443db4b0088907cb5995","OType":"uptongji","OData":""},{"TaskID":"652586369a0340259d8e48c240073e1a","OType":"uptongji","OData":""},{"TaskID":"307f4217ec3d4d5fbc47b39b277dd301","OType":"uptongji","OData":""},{"TaskID":"1736f11ca3954dccba41412d779db928","OType":"uptongji","OData":""},{"TaskID":"fb8289f1d9ac4af7b8f06c0e47478e01","OType":"uptongji","OData":""},{"TaskID":"7696eacd0513448e9dbf13106b8486a9","OType":"uptongji","OData":""},{"TaskID":"96b0b18d042b48b0af72731366659900","OType":"uptongji","OData":""},{"TaskID":"e260febebd4c42519e077909b4acb3fd","OType":"uptongji","OData":""},{"TaskID":"1d7f70e29e1149a99f5128f8f81c64fb","OType":"uptongji","OData":""},{"TaskID":"83cb09d354f64319bf40c47e3724f6e7","OType":"uptongji","OData":""},{"TaskID":"40bf1eb8afd54d28961a5cf0c7708687","OType":"uptongji","OData":""},{"TaskID":"f220a3aa84014a3794650d189ef70feb","OType":"uptongji","OData":""},{"TaskID":"5946f0a485fd43adaa46a0ef722de0b3","OType":"uptongji","OData":""},{"TaskID":"dfae5942103f417fa13094a36b5d3f42","OType":"addpeople","OData":"5F770A72E92F40C982F37D3315C027E8"},{"TaskID":"4e2976cab4874f2ab7ebdb1d961419d9","OType":"addpeople","OData":"1B1C75E159CB47CA8FF1A7573D0F4B89"},{"TaskID":"4e0302c192f244ce822aae9855260719","OType":"addpeople","OData":"cd5de36238da47d3900a0e0c71acbf4d"},{"TaskID":"bec55a3edca94f7ea13515b9fdfb7aa4","OType":"addpeople","OData":"f84c75b93a914ab68b45e3033486132f"},{"TaskID":"8efcbe15e5b742c9b97cfc800c708084","OType":"addpeople","OData":"9fc81d2f8a3849d3bf0a78a56c5ce3eb"},{"TaskID":"bde526a12ebc49c69dbdb41242101cba","OType":"addpeople","OData":"66A794B596A84F12AC2A54ECCBC7C6D7"},{"TaskID":"207471d4f94c44f3bd20ef80281aa808","OType":"addpeople","OData":"71696dc366094cadb27f1220eaf520c4"},{"TaskID":"848c81eb62f44517880aa987f7cdeb4e","OType":"addpeople","OData":"5F770A72E92F40C982F37D3315C027E8"},{"TaskID":"36c274e231ad46968cc73d0d6814acf7","OType":"addpeople","OData":"2D28B7A5189C4709ACF8C30088DB91A2"},{"TaskID":"71ad1cd21a88495fb506b8e9829bc4bb","OType":"editpeople","OData":"1B1C75E159CB47CA8FF1A7573D0F4B89"},{"TaskID":"3e4710ed37744e02b0ec5572e39e06fb","OType":"editpeople","OData":"1B1C75E159CB47CA8FF1A7573D0F4B89"},{"TaskID":"22de40fbfb7e43bcb0bc043f3206bf37","OType":"editpeople","OData":"2D28B7A5189C4709ACF8C30088DB91A2"},{"TaskID":"46acd6eb6ea84943a185f8b680a3df61","OType":"addpeople","OData":"1B1C75E159CB47CA8FF1A7573D0F4B89"},{"TaskID":"2ce7c46edf364aab8905af14fb5e6ac2","OType":"addpeople","OData":"cd5de36238da47d3900a0e0c71acbf4d"},{"TaskID":"e6418add4ca144db994163bfae93e2e7","OType":"addpeople","OData":"f84c75b93a914ab68b45e3033486132f"},{"TaskID":"58c828eb51cf4546be88c55c66f5d2b7","OType":"addpeople","OData":"71696dc366094cadb27f1220eaf520c4"},{"TaskID":"257a6b85ab9d409fa388758cb78cdd31","OType":"addpeople","OData":"5F770A72E92F40C982F37D3315C027E8"},{"TaskID":"010164918934413f863957d009b9476c","OType":"addpeople","OData":"2D28B7A5189C4709ACF8C30088DB91A2"},{"TaskID":"44d0a08698fb4a51af6bcc4585b2ff8c","OType":"addpeople","OData":"9fc81d2f8a3849d3bf0a78a56c5ce3eb"},{"TaskID":"5775aeecd7fc4e37acb0ec937e4eb170","OType":"addpeople","OData":"66A794B596A84F12AC2A54ECCBC7C6D7"},{"TaskID":"351261b4e0c340b899baf33970833ee3","OType":"editpeople","OData":"2D28B7A5189C4709ACF8C30088DB91A2"},{"TaskID":"d43e9f9acb7e4271ba791e9ee9ce5eca","OType":"editpeople","OData":"2D28B7A5189C4709ACF8C30088DB91A2"}]
     */

    /**
     * 任务id
     */
    private String TaskID;
    /**
     * 操作类型：
     * 1：查询成功 为新增设备，请求NewMacQuery接口提交设备信息
     * 2：查询成功 为已有设备且没有任务，无需后续操作
     * 3：同步时间
     * 6：设置音量，OData中为0-100音量数值
     * b：清理终端文件
     * d：上传截屏图片，请求UploadPrtScreen接口，OData中为 截屏id
     * g：重启终端
     * h：关闭终端
     * r：升级终端软件
     * t：清理终端文件
     * z：重启终端软件
     * Synctime：同步终端时间
     * uptongji：提交评价结果，请求UploadTongJi接口
     * addpeople：增加一条人员数据，请求GetYuanGong接口，OData中为人员id
     * delpeople：删除一条人员数据，请求GetYuanGong接口 ，OData中为人员id
     * editpeople：编辑一条人员数据，请求GetYuanGong接口，OData中为人员id
     */
    private String OType;
    private String OData;

    public String getTaskID() {
        return TaskID;
    }

    public void setTaskID(String TaskID) {
        this.TaskID = TaskID;
    }

    public String getOType() {
        return OType;
    }

    public void setOType(String OType) {
        this.OType = OType;
    }

    public String getOData() {
        return OData;
    }

    public void setOData(String OData) {
        this.OData = OData;
    }
}
