package com.showinfo.evaluation.entity;

/**
 * Copyright (C), 2015-2021
 *
 * @Date: 21-3-29 上午10:19
 * @author: seven
 * @ClassName: PersonList
 * @Description:
 **/
public class PersonList {
    public String name;
    public int index;

    public PersonList(String name, int index) {
        this.name = name;
        this.index = index;
    }
}
