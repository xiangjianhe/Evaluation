package com.showinfo.evaluation.entity;

import android.util.Log;

import com.showinfo.evaluation.EvaluationApplication;
import com.showinfo.evaluation.utils.SharefenceUtil;

import org.jetbrains.annotations.NotNull;
import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * Copyright (C), 2015-2021
 *
 * @Date: 21-3-16 下午2:53
 * @author: seven
 * @ClassName: Person1
 * @Description:
 **/
@Table(name = "parent")
public class Person {

    /**
     * ID : 7D4567928AD445229B2E3A32FA5BD196
     * BackupColumn :
     * CreateDate : null
     * Department : 22
     * Index_S : 1
     * Info : 阿斯顿害怕啥等哈说v爱仕达撒旦
     * MacID :
     * Name : 张三
     * Post : 业务员
     * Telphone : 1888888888
     * UpdateDate : /UpLoadFiles/image/80facac45e4d4e40a85b13c4185e2260.jpg
     */

    final public String url = "http://192.168.1.74:8112";

    @Column(name = "id", isId = true)
    private int id;
    /**
     * id
     */
    @Column(name = "personId")
    private String personId;
    /**
     * 分组序号
     */
    @Column(name = "backupColumn")
    private String backupColumn;
    /**
     * 创建时间
     */
    @Column(name = "createDate")
    private String createDate;
    /**
     * 部门
     */
    @Column(name = "department")
    private String department;
    /**
     * 员工序号
     */
    @Column(name = "indexS")
    private int indexS;
    /**
     * 介绍
     */
    @Column(name = "info")
    private String info;
    /**
     * --
     */
    @Column(name = "macId")
    private String macId;
    /**
     * 姓 名
     */
    @Column(name = "name")
    private String name;
    /**
     * 职务
     */
    @Column(name = "post")
    private String post;
    /**
     * 电话
     */
    @Column(name = "telphone")
    private String telphone;
    /**
     * 照片相对路径
     * http://192.168.174:8112  +   UpdateDate
     */
    @Column(name = "updateDate")
    private String updateDate;

    /**
     * 任务id
     */
    @Column(name = "taskId")
    private String taskId;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public int getBackupColumn() {
        return backupColumn == null ? 0 : Integer.parseInt(backupColumn);
    }

    public void setBackupColumn(String backupColumn) {
        this.backupColumn = backupColumn;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }


    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getIndexS() {
        return indexS;
    }

    public void setIndexS(int indexS) {
        this.indexS = indexS;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getMacId() {
        return macId;
    }

    public void setMacId(String macId) {
        this.macId = macId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String Post) {
        this.post = Post;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getUpdateDate() {
        String ip = SharefenceUtil.getIp(EvaluationApplication.getInstance());
        String duan = SharefenceUtil.getDuan(EvaluationApplication.getInstance());
        // 接口地址
        String uri = "http://" + ip + ":" + duan + updateDate;
        boolean is = false;
        if (updateDate != null) {
            is = updateDate.contains("http");
        }
        return is ? updateDate : uri;
    }

    public void setUpdateDate(String UpdateDate) {
        this.updateDate = UpdateDate;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return taskId;
    }

    @NotNull
    @Override
    public String toString() {
        return "Person{" +
                "url='" + url + '\'' +
                ", id=" + id +
                ", personId='" + personId + '\'' +
                ", backupColumn='" + backupColumn + '\'' +
                ", createDate='" + createDate + '\'' +
                ", department='" + department + '\'' +
                ", indexS=" + indexS +
                ", info='" + info + '\'' +
                ", macId='" + macId + '\'' +
                ", name='" + name + '\'' +
                ", post='" + post + '\'' +
                ", telphone='" + telphone + '\'' +
                ", updateDate='" + updateDate + '\'' +
                ", taskId='" + taskId + '\'' +
                '}';
    }
}
