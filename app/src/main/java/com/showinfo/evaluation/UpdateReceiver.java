package com.showinfo.evaluation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.showinfo.evaluation.activity.MainActivity;

/**
 * Copyright (C), 2015-2021
 *
 * @Date: 21-4-1 上午11:35
 * @author: seven
 * @ClassName: UpdateReceiver
 * @Description:
 **/
public class UpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent it = new Intent();
        it.setClass(context, MainActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(it);

    }
}