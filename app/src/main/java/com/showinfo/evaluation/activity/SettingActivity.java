package com.showinfo.evaluation.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;


import com.showinfo.evaluation.R;
import com.showinfo.evaluation.base.BaseActivity;
import com.showinfo.evaluation.utils.SharefenceUtil;

import java.util.Objects;


public class SettingActivity extends BaseActivity {

    private Context mContext;

    //需要的权限
    public String[] NEEDED_PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET};

    EditText dialog_courtName, dialog_pwd, et_text_ip, et_text_duankou;
    ImageView dialog_cancel, dialog_sure;
    RadioButton check_true, check_false;
    private String courtName = "";
    private String pinyin = "";
    private String pwd = "";
    private String ip = "";
    private String duankou = "";
    private Boolean isShow = true;
    private int screenTime = 180 * 1000;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {//返回屏保倒计时结束
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Objects.requireNonNull(intent.getAction()).equals("countDownTimer")) {
                SettingActivity.this.finish();
            }
        }
    };

    public CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        courtName = getIntent().getStringExtra("court");
        pinyin = getIntent().getStringExtra("pinyin");
        pwd = getIntent().getStringExtra("password");

        mContext = this;
        if (!"".equals(SharefenceUtil.getShowSupport(mContext))
                && SharefenceUtil.getShowSupport(mContext) != null) {
            isShow = SharefenceUtil.getShowSupport(mContext);
        } else {
            isShow = true;
        }

        if (!"".equals(SharefenceUtil.getIp(mContext)) && SharefenceUtil.getIp(mContext) != null) {
            ip = SharefenceUtil.getIp(mContext);
        } else {
            ip = getResources().getString(R.string.main_ip);
        }
        if (!"".equals(SharefenceUtil.getDuan(mContext)) && SharefenceUtil.getDuan(mContext) != null) {
            duankou = SharefenceUtil.getDuan(mContext);
        } else {
            duankou = getResources().getString(R.string.main_duankou);
        }

        initView();
        IntentFilter filter = new IntentFilter();
        filter.addAction("countDownTimer");
        registerReceiver(mReceiver, filter);
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        et_text_ip = findViewById(R.id.et_text_ip);
        et_text_ip.setText("" + ip);
        et_text_ip.setOnKeyListener(onKey);
        et_text_ip.addTextChangedListener(textWatcher);

        et_text_duankou = findViewById(R.id.et_text_duankou);
        et_text_duankou.setText("" + duankou);
        et_text_duankou.setOnKeyListener(onKey);
        et_text_duankou.addTextChangedListener(textWatcher);

//        dialog_pinyin = findViewById(R.id.dialog_pinyin);
//        dialog_pinyin.setText("" + pinyin);
//        dialog_pinyin.setOnKeyListener(onKey);
//        dialog_pinyin.addTextChangedListener(textWatcher);

        dialog_courtName = findViewById(R.id.dialog_courtName);
        dialog_courtName.setText("" + courtName);
        dialog_courtName.setOnKeyListener(onKey);
        dialog_courtName.addTextChangedListener(textWatcher);

        dialog_pwd = findViewById(R.id.dialog_pwd);
        dialog_pwd.setText("" + pwd);
        dialog_pwd.setOnKeyListener(onKey);
        dialog_pwd.addTextChangedListener(textWatcher);

        dialog_cancel = findViewById(R.id.dialog_cancel);
        dialog_sure = findViewById(R.id.dialog_sure);
        check_true = findViewById(R.id.check_true);
        check_false = findViewById(R.id.check_false);
        dialog_sure.setOnClickListener(this);
        dialog_cancel.setOnClickListener(this);
        check_true.setOnClickListener(this);
        check_false.setOnClickListener(this);
        if (isShow == true) {
            check_true.setChecked(true);
            check_false.setChecked(false);
        } else {
            check_true.setChecked(false);
            check_false.setChecked(true);
        }
    }


    View.OnKeyListener onKey = new View.OnKeyListener() {

        @Override

        public boolean onKey(View v, int keyCode, KeyEvent event) {

            // TODO Auto-generated method stub

            if (keyCode == KeyEvent.KEYCODE_ENTER) {

                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

                if (imm.isActive()) {

                    imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);

                }

                return true;

            }

            return false;

        }

    };

    @Override
    protected void onResume() {
        super.onResume();
        startAD();

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_UP:
                //抬起时启动定时
                startAD();
                break;
            //否则其他动作计时取消
            default:
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                }
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //有按下动作时取消定时
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            startAD();
        }
    };

    /**
     * 跳轉廣告
     */
    public void startAD() {
        if (countDownTimer == null) {
            countDownTimer = new CountDownTimer(screenTime * 1000, 1000l) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    Intent intent = new Intent(mContext, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            };
            countDownTimer.start();
        } else {
            countDownTimer.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //销毁时停止定时
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.check_true:
                isShow = true;
                break;
            case R.id.check_false:
                isShow = false;
                break;
            case R.id.dialog_sure:

                if (TextUtils.isEmpty(dialog_courtName.getText())) {
                    Toast.makeText(mContext, "请输入软件名称", Toast.LENGTH_SHORT).show();
                } /*else if (TextUtils.isEmpty(dialog_pinyin.getText())) {
                    Toast.makeText(mContext, "请输入拼音", Toast.LENGTH_SHORT).show();
                } */else if (TextUtils.isEmpty(dialog_pwd.getText())) {
                    Toast.makeText(mContext, "请输入密码", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(et_text_ip.getText())) {
                    Toast.makeText(mContext, "请输入IP地址", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(et_text_duankou.getText())) {
                    Toast.makeText(mContext, "请输入端口地址", Toast.LENGTH_SHORT).show();
                } else {
                    SharefenceUtil.savePassword(dialog_pwd.getText().toString(), mContext);
                    SharefenceUtil.saveTitleName(dialog_courtName.getText().toString(), mContext);
//                    SharefenceUtil.saveTitlePin(dialog_pinyin.getText().toString(), mContext);
                    SharefenceUtil.saveIp(et_text_ip.getText().toString(), mContext);
                    SharefenceUtil.saveDuan(et_text_duankou.getText().toString(), mContext);
                    SharefenceUtil.saveShowSopport(isShow, mContext);

                    Log.d("seven", "SettingActivity-->>onClick: " + et_text_ip.getText().toString());
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    SettingActivity.this.finish();
                }

                break;
            case R.id.dialog_cancel:
                SettingActivity.this.finish();
                break;
            default:
                break;
        }
    }

    /**
     * 隐藏键盘
     */
    protected void hideInput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        View v = getWindow().peekDecorView();
        if (null != v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
        //销毁时停止定时
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    protected String[] needPermissions() {
        return NEEDED_PERMISSIONS;
    }

    @Override
    protected void afterPermissions() {
//        init();
    }
}