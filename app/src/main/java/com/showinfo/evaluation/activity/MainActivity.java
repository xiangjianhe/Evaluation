package com.showinfo.evaluation.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONObject;
import com.showinfo.evaluation.R;
import com.showinfo.evaluation.adapter.LeftBindingAdapter;
import com.showinfo.evaluation.adapter.RightBindingAdapter;
import com.showinfo.evaluation.base.BaseActivity;
import com.showinfo.evaluation.databinding.ActivityMainBinding;
import com.showinfo.evaluation.entity.Person;
import com.showinfo.evaluation.entity.PersonList;
import com.showinfo.evaluation.entity.ShutdownTime;
import com.showinfo.evaluation.http.callback.IConnect;
import com.showinfo.evaluation.update.AutoUpdateUtil;
import com.showinfo.evaluation.utils.LogToFile;
import com.showinfo.evaluation.utils.NetUtils;
import com.showinfo.evaluation.utils.SharefenceUtil;
import com.showinfo.evaluation.utils.Utils;
import com.showinfo.evaluation.utils.task.Task;
import com.showinfo.evaluation.utils.task.TimeHandler;
import com.showinfo.evaluation.utils.task.TimeTask;
import com.showinfo.evaluation.view.Tips;
import com.showinfo.evaluation.view.ToastManager;

import org.xutils.DbManager;
import org.xutils.ex.DbException;

import org.xutils.x;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import static com.showinfo.evaluation.entity.ShutdownTime.week;
import static com.showinfo.evaluation.http.NetRequestTools.WcfMacMain;
import static com.showinfo.evaluation.http.NetRequestTools.daoConfig;
import static com.showinfo.evaluation.http.NetRequestTools.getCountResult;
import static com.showinfo.evaluation.utils.NetUtils.getLocalIp;
import static com.showinfo.evaluation.utils.SharefenceUtil.getPassword;
import static com.showinfo.evaluation.utils.Utils.dataOne;
import static com.showinfo.evaluation.utils.Utils.shutdown;

/**
 * Copyright (C), 2015-2021
 *
 * @Date: 21-3-2 上午10:50
 * @author: seven
 * @ClassName: MainActivity
 * @Description:
 **/
public class MainActivity extends BaseActivity implements RightBindingAdapter.OnItemClickListener, IConnect {
    private ActivityMainBinding mBinding;

    public static Context mContext;
    //需要的权限
    public String[] NEEDED_PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET};

    private LeftBindingAdapter leftAdapter;
    private RightBindingAdapter rightAdapter;
    private RecyclerView leftRecycle;

    private List<Person> leftList = new ArrayList<>();
    private List<PersonList> rightList = new ArrayList<>();
    private DbManager db = null;

    private Handler mHandler;
    private int time = 0;
    private String mac = "";
    private static int width;
    private static int height;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1000:

                    break;
                default:
                    break;

            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.init(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mContext = this;
        mac = NetUtils.getMac(mContext);
        WcfMacMain(mac, this);
        initClicked();
        initRecycle();

        //分辨率 只有整数
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        //计算出来是799.5 用Math.round 四舍五入 整数800
        width = Math.round(dm.widthPixels * dm.density);
        height = Math.round(dm.heightPixels * dm.density);
    }

    private void initClicked() {
        mBinding.searchEdit.setOnKeyListener(onKey);
        mBinding.searchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //当actionId == XX_SEND 或者 XX_DONE时都触发
                //或者event.getKeyCode == ENTER 且 event.getAction == ACTION_DOWN时也触发
                //注意，这是一定要判断event != null。因为在某些输入法上会返回null。
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || (event != null && KeyEvent.KEYCODE_ENTER == event.getKeyCode() && KeyEvent.ACTION_DOWN == event.getAction())) {
                    //处理事件
                    mBinding.searchEdit.setFocusable(false);
                    doSearch();
                }
                return false;
            }
        });

        //点击搜索按钮
        mBinding.btnSearch.setOnClickListener(v -> {

            String search = mBinding.searchEdit.getText().toString();
            if (!search.equals("")) {
                doSearch();
                mBinding.btnSearch.setVisibility(View.GONE);
                mBinding.btnSearchCancel.setVisibility(View.VISIBLE);
            }

        });
        //取消搜索
        mBinding.btnSearchCancel.setOnClickListener(v -> {
            mBinding.searchEdit.setText("");
            doSearch();
            mBinding.btnSearchCancel.setVisibility(View.GONE);
            mBinding.btnSearch.setVisibility(View.VISIBLE);
        });
        //长按法院标题显示密码输入框
        mBinding.mainTitle.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                passWord();

                return false;
            }
        });
        mBinding.btTypeText.setText("全  部");
        mBinding.linearLayout8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("seven", "MainActivity-->>onClick: " + mBinding.recycleRight.getVisibility());
                if (mBinding.recycleRight.getVisibility() == View.VISIBLE) {
                    mBinding.recycleRight.setVisibility(View.GONE);
                    mBinding.iconArrow.setBackgroundResource(R.drawable.arrow_b);
                } else {
                    mBinding.recycleRight.setVisibility(View.VISIBLE);
                    mBinding.iconArrow.setBackgroundResource(R.drawable.arrow_r);
                }
            }
        });
    }

    @Override
    protected String[] needPermissions() {
        return NEEDED_PERMISSIONS;
    }

    @Override
    protected void afterPermissions() {
//        init();

    }

    @Override
    protected void onResume() {
        super.onResume();
        LogToFile.e("onResume");
        getCountResult(mBinding.textFavorableRate);
        try {
            db = x.getDb(daoConfig);
        } catch (DbException e) {
            e.printStackTrace();
            return;
        }

        //定时任务，每15秒在线程中执行一次
        mHandler = new Handler();
        mHandler.post(run);

        initView();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void initView() {
//        setFormatHour(mBinding.clockWeek, "EE");
//        setFormatHour(mBinding.clockDay, "yyyy/MM/dd");
//        setFormatHour(mBinding.clockHour, "HH:mm");

        mBinding.mainTitle.setText(SharefenceUtil.getTitleName(mContext));
//        mBinding.mainPin.setText(SharefenceUtil.getCourtPin(mContext));
        boolean isShow = SharefenceUtil.getShowSupport(mContext);
//        mBinding.activityMain.setBackgroundResource(isShow ? R.drawable.bg_evaluate_support : R.drawable.bg_evaluate_no_support);
        mBinding.textTechnicalSupport.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    /**
     * 初始化适配器
     */
    private void initRecycle() {
        getData();
        leftAdapter = new LeftBindingAdapter(MainActivity.this, this);
        leftRecycle = findViewById(R.id.recycle_left);
        leftRecycle.setLayoutManager(new LinearLayoutManager(this));
        leftRecycle.setAdapter(leftAdapter);
        //设置数据
        leftAdapter.setList(leftList);

        rightAdapter = new RightBindingAdapter(this, this);
        RecyclerView rightRecycle = findViewById(R.id.recycle_right);
        rightRecycle.setLayoutManager(new LinearLayoutManager(this));
        rightRecycle.setAdapter(rightAdapter);


        rightAdapter.setList(rightList);

    }

    /**
     * 获取适配器的数据
     */
    private void getData() {

        leftList.clear();
        rightList.clear();

        try {
            if (db != null) {
                leftList = db.selector(Person.class).orderBy("id", true).limit(100).findAll();
            }
            if (leftList == null) {
                leftList = new ArrayList<>();
            }
        } catch (DbException e) {
            e.printStackTrace();
        }

        if (leftList != null && leftList.size() != 0) {
            rightList.add(new PersonList("全  部", 0));
            for (int i = 0; i < leftList.size(); i++) {
                Person person = leftList.get(i);

                LogToFile.d("seven", "MainActivity-->>initRecycle: " + person.getDepartment());
                if (i == 0) {
                    rightList.add(new PersonList(person.getDepartment(), person.getBackupColumn()));
                } else {
                    int count = 0;
                    // 遍历集合，依次取出其中的元素
                    for (int j = 0; j < rightList.size(); j++) {

                        // 如果字符串参数作为一个子字符串在此对象中出现，则返回第一个这种子字符串的第一个字符的索引；如果它不作为一个子字符串出现，则返回
                        // -1。
                        if (person.getDepartment().contains(rightList.get(j).name)) {
                            count++;
                        }
                    }
                    if (count == 0) {
                        rightList.add(new PersonList(person.getDepartment(), person.getBackupColumn()));
                    }

                }
            }
            //按照序号排序
            Collections.sort(rightList, new Comparator<PersonList>() {
                @Override
                public int compare(PersonList t1, PersonList t2) {
                    // TODO Auto-generated method stub
                    if (t1.index != 0 && t2.index != 0) {
                        if (t1.index > t2.index) {
                            return 1;
                        }
                        if (t1.index < t2.index) {
                            return -1;
                        }
                    } else {
                        return 1;
                    }
                    return 0;
                }
            });
            getList();
        }
        if (rightList == null) {
            rightList = new ArrayList<>();
        }
    }

    /**
     * 对列表重新排列
     */
    private void getList() {
        List<Person> list = new ArrayList<>();
        for (PersonList personList : rightList) {
            List<Person> people = new ArrayList<>();
            for (Person person : leftList) {
                if (person.getDepartment().equals(personList.name)) {
                    people.add(person);
                }
            }
            //按照序号排序
            Collections.sort(people, new Comparator<Person>() {
                @Override
                public int compare(Person t1, Person t2) {
                    // TODO Auto-generated method stub
                    if (t1.getIndexS() != 0 && t2.getIndexS() != 0) {
                        if (t1.getIndexS() > t2.getIndexS()) {
                            return 1;
                        }
                        if (t1.getIndexS() < t2.getIndexS()) {
                            return -1;
                        }
                    } else {
                        return 1;
                    }
                    return 0;
                }
            });
            list.addAll(people);
        }
        leftList.clear();
        leftList.addAll(list);
    }

    @Override
    public void dataChange() {
        // UI同步执行
        x.task().post(this::initRecycle);

    }

    @Override
    public void addShutdown() {

        try {
            DbManager db = x.getDb(daoConfig);

            List<ShutdownTime> times = db.findAll(ShutdownTime.class);
            if (times == null || times.size() == 0) {
                return;
            }
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("EE");
            long time = System.currentTimeMillis();
            Date date = new Date(time);
            String today = format.format(date);

            Log.e("seven", "time=" + format.format(date));

            List<MyTask> myTasks = new ArrayList<>();

            for (ShutdownTime shutdownTime : times) {
                Log.e("seven", "MainActivity --->> addShutdown: " + shutdownTime.toString());
                if (today.equals(shutdownTime.getWeekday())) {
                    MyTask myTask = new MyTask();
                    myTask.shutdownTime = shutdownTime;
                    myTasks.add(myTask);
                    format = new SimpleDateFormat("yyyy-MM-dd");
                    myTask.setStarTime(dataOne(format.format(date) + " " + shutdownTime.getTime() + ":00"));
                    myTask.setEndTime(dataOne(format.format(date) + " " + shutdownTime.getTime() + ":10"));
                }
            }

            if (myTasks.size() == 0) {
                return;
            }
            // TODO: 2017/11/8  创建一个任务处理器
            myTaskTimeTask = new TimeTask<>(MainActivity.this, ACTION_SHUTDOWN);

            // TODO: 2017/11/8   添加时间回掉
            myTaskTimeTask.addHandler(timeHandler);
            // TODO: 2017/11/8 把资源放进去处理
            myTaskTimeTask.setTasks(myTasks);
            myTaskTimeTask.startLooperTask();
        } catch (DbException e) {
            e.printStackTrace();
        }


    }

    final String ACTION_SHUTDOWN = "timeTask.action.shutdown";

    private TimeTask<MyTask> myTaskTimeTask;

    TimeHandler<MyTask> timeHandler = new TimeHandler<MyTask>() {
        @Override
        public void exeTask(MyTask mTask) {

            shutdown();
            Log.i("seven", "MainActivity --->> exeTask: 到点了" + mTask.shutdownTime.toString());

        }

        @Override
        public void overdueTask(MyTask mTask) {
            Log.i("seven", "MainActivity --->> overdueTask: 过期了" + mTask.shutdownTime.toString());
        }

        @Override
        public void futureTask(MyTask mTask) {

            Log.i("seven", "MainActivity --->> futureTask: 定时" + mTask.shutdownTime.toString());

        }
    };

    static class MyTask extends Task {
        //// TODO: 2021/04/20 这里可以放置你自己的资源,务必继承Task对象

        ShutdownTime shutdownTime;


    }


    @Override
    public void doClean() {
        x.task().post(this::initRecycle);

    }

    /**
     * 搜索关键词
     */
    private void doSearch() {
//        mBinding.searchEdit.clearFocus();
        String search = mBinding.searchEdit.getText().toString();
        List<Person> searchList = new ArrayList<>();
        for (Person person : leftList) {
            if (person.getName().contains(search)) {
                searchList.add(person);
            }
        }
        leftAdapter.setList(searchList);
        hideSoftKeyboard(this);
    }


    @Override
    public void select(int position, String string) {
        if (mBinding.recycleRight.getVisibility() == View.VISIBLE) {
            mBinding.recycleRight.setVisibility(View.GONE);
            mBinding.iconArrow.setBackgroundResource(R.drawable.arrow_b);
        } else {
            mBinding.recycleRight.setVisibility(View.VISIBLE);
            mBinding.iconArrow.setBackgroundResource(R.drawable.arrow_r);
        }
        mBinding.btTypeText.setText(string);
        Log.d("seven", "MainActivity-->>select: position " + position + " string is " + string);
        if (position == 0) {
            leftAdapter.setList(leftList);
            mBinding.searchEdit.setText("");
        } else {
            List<Person> people = new ArrayList<>();
            for (Person person : leftList) {
                if (person.getDepartment().equals(string)) {
                    people.add(person);
                }
            }
            //按照序号排序
            Collections.sort(people, new Comparator<Person>() {
                @Override
                public int compare(Person t1, Person t2) {
                    // TODO Auto-generated method stub
                    if (t1.getIndexS() != 0 && t2.getIndexS() != 0) {
                        if (t1.getIndexS() > t2.getIndexS()) {
                            return 1;
                        }
                        if (t1.getIndexS() < t2.getIndexS()) {
                            return -1;
                        }
                    } else {
                        return 1;
                    }
                    return 0;
                }
            });

            leftAdapter.setList(people);
        }
        leftRecycle.scrollToPosition(0);

    }


    Runnable run = new Runnable() {
        @Override

        public void run() {
            // TODO Auto-generated method stub

            getCountResult(mBinding.textFavorableRate);
            WcfMacMain(mac, MainActivity.this);
            if (leftList != null && leftList.size() == 0) {
                initRecycle();
            }

            addShutdown();

            LogToFile.d("seven", "MainActivity-->>run: " + time++);
            mHandler.postDelayed(this, 15000);

        }

    };

    @Override
    protected void onPause() {
        super.onPause();
        if (mHandler != null) {
            mHandler.removeCallbacks(run);
        }
    }

    @Override
    public void commonConectResult(String result, int connectType) {
        if (result.equals("") || result.equals("404")) {
            ToastManager.showInLong(this, "请求失败,请检查网络连接!------->>>>错误代码：" + result);
        } else {
            JSONObject json2 = JSONObject.parseObject(result);
            if (json2.getString("code").equals("0")) {
                x.task().post(new Runnable() { // UI同步执行
                    @Override
                    public void run() {
                        Tips.showShort("评价成功，感谢您的支持！");
                    }
                });
            }
            LogToFile.d("seven", "MainActivity-->>commonConectResult: " + json2.toString());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacks(run);
        }
        LogToFile.d("seven", "MainActivity-->>onDestroy: ");
        AutoUpdateUtil.getInstance(this).destroy();
    }


    private void passWord() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_password, null);
        EditText password = view.findViewById(R.id.edit_password);
        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        TextView sure = view.findViewById(R.id.sure);
        TextView cancel = view.findViewById(R.id.cancel);
        final Dialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setContentView(view);
        //使editext可以唤起软键盘
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getPassword(mContext).equals(password.getText().toString())) {
                    Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                    intent.putExtra("court", mBinding.mainTitle.getText().toString());
//                    intent.putExtra("pinyin", mBinding.mainPin.getText().toString());
                    intent.putExtra("password", "" + password.getText().toString());
                    startActivity(intent);
                    hideInput();
                    dialog.dismiss();
                } else {
                    Toast.makeText(MainActivity.this, "请输入正确的密码", Toast.LENGTH_SHORT).show();
                }
            }

        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!CommonTool.isFastDoubleClick()) {
                hideInput();
                dialog.dismiss();
//                }
            }
        });
    }

    /**
     * 隐藏键盘
     */
    protected void hideInput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        View v = getWindow().peekDecorView();
        if (null != v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }


    View.OnKeyListener onKey = new View.OnKeyListener() {

        @Override

        public boolean onKey(View v, int keyCode, KeyEvent event) {

            // TODO Auto-generated method stub

            if (keyCode == KeyEvent.KEYCODE_ENTER) {

                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                v.setFocusable(false);
                if (imm.isActive()) {

                    imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
                }

                return true;

            }

            return false;

        }

    };

    /**
     * 获取分辨率的宽
     *
     * @return
     */
    public static int getWith() {
        return width;
    }

    /**
     * 获取分辨率的高
     *
     * @return
     */
    public static int getHeight() {
        return height;
    }

    /**
     * 设置时钟
     *
     * @param v
     * @param format
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setFormatHour(TextClock v, String format) {
        if (v.is24HourModeEnabled()) {
            v.setFormat24Hour(format);
        } else {
            v.setFormat12Hour(format);
        }
    }
}