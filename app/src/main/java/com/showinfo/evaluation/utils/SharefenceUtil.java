package com.showinfo.evaluation.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.showinfo.evaluation.R;


public class SharefenceUtil {

    private static final String PASSWORD = "123456";
    //无人操作返回的时间
    public static final int DEFAULT_SCREEN_TIME_ = 180 * 1000;//定时跳转时间

    private static final String KEY_DOORPLATE = "key_doorplate";
    private static final String KEY_TOKEN = "key_token";
    private static final String KEY_BEAN = "key_bean";
    private static final String KEY_TIMES = "key_times";
    private static final String KEY_PASSWORD = "key_password";
    private static final String KEY_COURTNAME = "key_courtname";
    private static final String KEY_TITLE = "key_title";
    private static final String KEY_PINYIN = "key_pinyin";
    private static final String KEY_IP = "key_ip";
    private static final String KEY_DUANKOU = "key_duankou";
    private static final String KEY_COURT = "key_court";
    private static final String KEY_SHOW = "key_show";
    private static final String KEY_TIME = "key_time";


    public static void savePassword(String url, Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String str = url;
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_PASSWORD, url);
        editor.commit();
    }

    public static String getPassword(Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String listJson = sp.getString(KEY_PASSWORD, "");
        return listJson.equals("") ? PASSWORD : listJson;
    }

    public static void saveTitleName(String url, Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String str = url;
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_TITLE, url);
        editor.commit();
    }

    public static String getTitleName(Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String listJson = sp.getString(KEY_TITLE, "");
        return listJson.equals("") ? context.getResources().getString(R.string.main_title) : listJson;
    }

    public static void saveTitlePin(String url, Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String str = url;
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_PINYIN, url);
        editor.commit();
    }

    public static String getCourtPin(Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String listJson = sp.getString(KEY_PINYIN, "");
        return listJson.equals("") ? context.getResources().getString(R.string.main_pinyin) : listJson;
    }

    public static void saveIp(String url, Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String str = url;
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_IP, url);
        editor.commit();
    }

    public static String getIp(Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String listJson = sp.getString(KEY_IP, "");
        Log.d("seven", "SharefenceUtil-->>getIp: " + listJson);
        return "".equals(listJson) ? context.getResources().getString(R.string.main_ip) : listJson;
    }

    public static void saveDuan(String url, Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String str = url;
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_DUANKOU, url);
        editor.commit();
    }

    public static String getDuan(Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String listJson = sp.getString(KEY_DUANKOU, "");
        return listJson.equals("") ? context.getResources().getString(R.string.main_duankou) : listJson;
    }

    public static void saveCourtName(String url, Context context) {
        Log.d("seven", "SharefenceUtil-->>saveCourtName: " + url);
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String str = url;
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_COURT, url);
        editor.commit();
    }

    public static String getCourtName(Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        String listJson = sp.getString(KEY_COURT, "");
        return listJson;
    }

    public static void saveShowSopport(Boolean url, Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        Boolean str = url;
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_SHOW, url);
        editor.commit();
    }

    public static Boolean getShowSupport(Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        Boolean listJson = sp.getBoolean(KEY_SHOW, true);
        return listJson;
    }

    public static void saveScreenTime(int url, Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        int str = url;
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(KEY_TIME, url);
        editor.commit();
    }

    public static int getScreenTime(Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        int listJson = sp.getInt(KEY_TIME, DEFAULT_SCREEN_TIME_);
        return listJson;
    }

    public static void reset(Context context) {
        SharedPreferences sp = context.getSharedPreferences(KEY_DOORPLATE, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear().apply();
    }
}
